//
//  CountryListTableViewController.swift
//  CurrencyConvertor
//
//  Created by Kaodim MacMini on 21/01/2021.
//

import UIKit
import Combine

class CountryListTableViewController: UITableViewController {

    let selectedCurreny = PassthroughSubject<SelectedCurrency, Never>()
    var selection: CurrencySelectionType = .primary

    private let currencies = Currency.currencySections
    // Table view cells are reused and should be dequeued using a cell identifier.
    let cellIdentifier = "Cell"

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "SelectCurrency".localized()
        navigationItem.largeTitleDisplayMode = .always
        // Uncomment the following line to preserve selection between presentations
        self.clearsSelectionOnViewWillAppear = false

    }
    // MARK: - Table view data source and delegates

    override func numberOfSections(in tableView: UITableView) -> Int {
        return currencies.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = currencies[section]
        return section.currencies.count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CountryTableCell else {
            fatalError("The dequeued cell is not an instance of CountryTableCell.")
        }
        let section = currencies[indexPath.section]
        let currency = section.currencies[indexPath.row]
        let data = CountryViewViewModel(currency)
        cell.configureUI(data)
        return cell
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let section = currencies[section]
        return section.title
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = currencies[indexPath.section]
        let currency = section.currencies[indexPath.row]
        selectedCurreny.send((currency,selection))
        self.dismiss(animated: true, completion: nil)
    }
}
