//
//  CurrencyConvertorViewController.swift
//  CurrencyConvertor
//
//  Created by Kaodim MacMini on 21/01/2021.
//

import UIKit
import Combine


enum CurrencySelectionType {
    case primary
    case secondary
}

class CurrencyConvertorViewController: UIViewController {

    private var subscriptions = Set<AnyCancellable>()
    var items: [Currency] = []
    var viewModel = CurrencyConvertorViewModel()
    var bindings: [Binding<CurrencyConvertorViewModel>] = []

    var refreshBarButtonActivityIndicator: UIBarButtonItem!
    var refreshBarButton: UIBarButtonItem!
    var activityIndicator: UIActivityIndicatorView!

    @IBOutlet weak var primaryCurrencyTextField: UITextField!
    @IBOutlet weak var primaryCurrencyLabel: UILabel!
    @IBOutlet weak var secondaryCurrencyTextField: UITextField!
    @IBOutlet weak var secondaryCurrencyLabel: UILabel!


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        bindTextField()
        bindNetworkUpdate()

        barItemSetUp()

        let primaryText = primaryCurrencyTextField.bindText(to: viewModel, on: \.primaryValue)
        bindings.append(primaryText)
    }

    func barItemSetUp() {
        let image = UIImage(systemName: "arrow.clockwise")?.withRenderingMode(.alwaysOriginal)
        refreshBarButton = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(refreshNow))
        activityIndicator = UIActivityIndicatorView.init(style: .medium)
        refreshBarButtonActivityIndicator = UIBarButtonItem(customView: activityIndicator)
    }

    @objc func refreshNow() {
        self.navigationItem.rightBarButtonItem = refreshBarButtonActivityIndicator
        activityIndicator.startAnimating()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.viewModel.reFetchCurrencyRates()
        }
    }

    func bindNetworkUpdate() {
        viewModel.$ratesFetched
            .handleEvents(receiveOutput: { [unowned self] isFetched in
                if isFetched {
                    self.activityIndicator.stopAnimating()
                    self.navigationItem.rightBarButtonItem = nil
                }
            })
            .sink { _ in }
            .store(in: &subscriptions)

        viewModel.$error
            .debounce(for: .milliseconds(500), scheduler: RunLoop.main)
            .handleEvents(receiveOutput: { [unowned self] error in
                if error != nil {
                    showAlert(error?.localizedDescription ?? "")
                    reloadRefreshBtn(true)
                }
                
            })
            .sink { _ in }
            .store(in: &subscriptions)
    }

    func reloadRefreshBtn(_ show: Bool)  {
        self.navigationItem.rightBarButtonItem = show ? self.refreshBarButton: self.refreshBarButtonActivityIndicator
    }

    func showAlert(_ error: String) {
        let alert = UIAlertController.init(title: "Error", message: error, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Ok", style: .default, handler: { _ in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }

    @IBAction func primaryCurrencyTapped(_ sender: Any) {
        navigateToCountryListVC(.primary)
    }


    @IBAction func secondaryCurrencyTapped(_ sender: Any) {
        navigateToCountryListVC(.secondary)
    }

    func bindTextField() {
        // Bind textfield with exchange to update currency appropriately
        viewModel.$exchange
            .handleEvents(receiveOutput: { [unowned self] exchange in
                updateExchangeRates(exchange)
            })
            .sink { _ in }
            .store(in: &subscriptions)
    }

    func updateExchangeRates(_ exchange: Exchange) {
        primaryCurrencyTextField.text = ""
        secondaryCurrencyTextField.text = ""

        primaryCurrencyTextField.text = exchange.primaryValueDisplay
        primaryCurrencyLabel.text = exchange.primary.fullName

        secondaryCurrencyTextField.text = exchange.secondaryValueDisplay
        secondaryCurrencyLabel.text = exchange.secondary.fullName
    }

    // Navigate to country list
    func navigateToCountryListVC(_ selectionType: CurrencySelectionType) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyBoard.instantiateViewController(withIdentifier: "CountryListTableViewController") as! CountryListTableViewController
        controller.selection = selectionType
        controller.selectedCurreny
            .handleEvents(receiveOutput: { [unowned self] selectedCurrencyInfo in
                viewModel.updateExchange(selectedCurrencyInfo)
            })
            .sink { _ in }
            .store(in: &subscriptions)

        let navController = UINavigationController.init(rootViewController: controller)
        navController.navigationBar.prefersLargeTitles = true
        self.present(navController, animated: true, completion: nil)
    }
}

