//
//  DatabaseManager.swift
//  CurrencyConvertor
//
//  Created by Kaodim MacMini on 21/01/2021.
//

import Foundation
import SwiftUI
import CoreData

class DatabaseManager {

    var managedObjectContext: NSManagedObjectContext?
    var fetchedRates: Set<Rate> = []

    init(_ fetchedRates: Set<Rate> = []) {

        self.fetchedRates = fetchedRates
        //As we know that container is set up in the AppDelegates so we need to refer that container.
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        //We need to create a context from this container
        managedObjectContext = appDelegate.persistentContainer.viewContext
    }

    public func setFetehedResult(fetchedRates: Set<Rate>) {
        self.fetchedRates = fetchedRates
    }

    public func getManagedRate() -> [ManagedRate] {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ManagedRate")
        do {
            let result = try managedObjectContext?.fetch(request)
            return result as? [ManagedRate] ?? []
        } catch {
            return []
        }
    }

    public func getManagedCurrencyRates() -> [ManagedCurrencyRate] {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ManagedCurrencyRate")
        do {
            let result = try managedObjectContext?.fetch(request)
            return result as? [ManagedCurrencyRate] ?? []
        } catch {
            return []
        }
    }
}


extension DatabaseManager {
    public func updateDataBase() {
        clearExistingRates()
        storeRatesLocally()
    }

    private func clearExistingRates() {

        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ManagedRate")
        do {
            let result = try managedObjectContext?.fetch(request)
            result?.forEach { rate in
                managedObjectContext?.delete(rate as! NSManagedObject)
            }
        } catch {
          
        }

        do {
            try managedObjectContext?.save()
        } catch {
            print(error.localizedDescription)
        }
    }

    private func storeRatesLocally() {
        guard let managedObjectContext = managedObjectContext else { return }
        fetchedRates.forEach { rate in
            let managedRate = ManagedRate(context: managedObjectContext)
            managedRate.id = rate.id
            managedRate.base = rate.base

            rate.rates.forEach { key, value in
                let managedCurrencyRate = ManagedCurrencyRate(context: managedObjectContext)
                managedCurrencyRate.ofRate = managedRate
                managedCurrencyRate.name = key
                managedCurrencyRate.value = NSDecimalNumber(decimal: value)
                managedCurrencyRate.rateId = rate.id
            }

            do {
                try self.managedObjectContext?.save()
            } catch {
                print(error.localizedDescription)
            }
        }
    }
}
