//
//  CurrencyConvertorViewModel.swift
//  CurrencyConvertor
//
//  Created by Kaodim MacMini on 21/01/2021.
//

import Foundation
import Combine
import SwiftUI

typealias SelectedCurrency = (Currency, CurrencySelectionType)

class CurrencyConvertorViewModel: ObservableObject {
    var cancellationToken: Set<AnyCancellable?> = []
    var fetchedRates: Set<Rate> = []

    @Published private(set) var error: Error? = nil
    @Published var ratesFetched: Bool = false

    var subscriptions = Set<AnyCancellable>()

    @FetchRequest(entity: ManagedRate.entity(), sortDescriptors: []) var managedRates: FetchedResults<ManagedRate>
    @FetchRequest(entity: ManagedCurrencyRate.entity(), sortDescriptors: []) var managedCurrencyRates: FetchedResults<ManagedCurrencyRate>

    let database: DatabaseManager!

    @Published var primaryValue: String? = "0.0" {
        didSet {
            exchange.currentValue = Decimal(string: primaryValue ?? "0.0") ?? 0.0
        }
    }

    @Published var exchange: Exchange = Exchange(
        primary: Currency(name: "MYR", fullName: "MalaysianRinggit".localized(), continent: .Asia),
        secondary: Currency(name: "USD", fullName: "UnitedStatesDollars".localized(), continent: .NorthAmerica)
    )

    init(database: DatabaseManager = DatabaseManager()) {
        self.database = database
        bindViewModel()
        fetchCurrencyRates()
    }

    public func getfetchedRates() -> Set<Rate> {
        fetchedRates
    }

    public func getExchange() -> Exchange {
        exchange
    }

    public func updateExchange(_ selectedCurrencyInfo: SelectedCurrency) {
        let selectionType = selectedCurrencyInfo.1
        let currency = selectedCurrencyInfo.0

        if selectionType == .primary {
            exchange.primary = currency
        } else {
            exchange.secondary = currency
        }

        self.updateExchanges()
        self.exchange.refreshConverstion()
    }

    private func updateExchanges() {
        updatePrimaryExchangeRate()
        updateSecondaryExchangeRate()
    }

    private func updatePrimaryExchangeRate() {
        let managedRates = database.getManagedRate()
        let managedCurrencyRates = database.getManagedCurrencyRates()

        guard let pmr = (managedRates.first { r in r.base == exchange.primary.name }) else {
            return
        }
        let pmrRates = managedCurrencyRates.filter { cr in cr.rateId == pmr.id }
        exchange.primaryRate = Rate.managedRateAsRate(rate: pmr, currencyRates: pmrRates)
    }

    private func updateSecondaryExchangeRate() {
        let managedRates = database.getManagedRate()
        let managedCurrencyRates = database.getManagedCurrencyRates()

        guard let smr = (managedRates.first { r in r.base == exchange.secondary.name }) else {
            return
        }
        let smrRates = managedCurrencyRates.filter { cr in cr.rateId == smr.id }
        exchange.secondaryRate = Rate.managedRateAsRate(rate: smr, currencyRates: smrRates)
    }

    func bindViewModel() {
        self.$ratesFetched
            .handleEvents(receiveOutput: { [unowned self] result in
                if result {
                    // Update currency to local database and current exchange
                    DispatchQueue.main.async {
                        database.setFetehedResult(fetchedRates: fetchedRates)
                        database.updateDataBase()
                        self.updateExchanges()
                    }
                }
            })
            .sink { _ in }
            .store(in: &subscriptions)
    }
}

extension CurrencyConvertorViewModel {

    func reFetchCurrencyRates() {
        ratesFetched = false
        error = nil
        fetchCurrencyRates()
    }

    private func fetchCurrencyRates() {
        Currency.currencies.forEach { currency in
            getLatestRateForBase(base: currency.name)
        }
    }

    private func getLatestRateForBase(base: String) {
        cancellationToken.insert(ExchangeRatesService.request(.latest, parameters: ["base": base])
                .mapError({ (error) -> Error in
                    self.error = error
                    // Update exchange with locally saved rates
                    self.updateExchanges()
                    return error
                })
                .sink(receiveCompletion: { _ in },
                    receiveValue: { rate in
                        self.fetchedRates.insert(rate)

                        // Trigger to updates currency rates after all currencies are updated.
                        if self.fetchedRates.count >= Currency.currencies.count {
                            self.ratesFetched = true
                        }
                    }))
    }
}
