//
//  Binding.swift
//  CurrencyConvertor
//
//  Created by Kaodim MacMini on 21/01/2021.
//

import Foundation
import UIKit

class Binding<Destination: AnyObject> {
    weak var source: UITextField?
    weak var destination: Destination?
    var property: WritableKeyPath<Destination, String?>

    init(source: UITextField?,
         destination: Destination?,
         property: WritableKeyPath<Destination, String?>) {
        self.source = source
        self.destination = destination
        self.property = property

        self.source?.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
    }

    @objc func textFieldDidChange() {
        destination?[keyPath: property] = source?.text
    }

    // 👩🏽‍💻👨🏼‍💻 Implement `class Binding`
}

extension UITextField {
    func bindText<Destination>(to destination: Destination, on property: WritableKeyPath<Destination, String?>) -> Binding<Destination> {
        // 👩🏽‍💻👨🏼‍💻 Implement `func bindText()`
        return Binding<Destination>.init(source: self, destination: destination, property: property)
    }
}
