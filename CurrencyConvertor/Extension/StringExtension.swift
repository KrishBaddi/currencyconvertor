//
//  StringExtension.swift
//  CurrencyConvertor
//
//  Created by Kaodim MacMini on 21/01/2021.
//

import Foundation
extension String {

    ////
    /// Returns a localised version for given string
    /// - Parameter comment:
    /// - Returns:
    func localized(withComment comment: String? = nil) -> String {
        NSLocalizedString(self, comment: comment ?? "")
    }

}
