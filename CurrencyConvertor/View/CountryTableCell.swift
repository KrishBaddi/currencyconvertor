//
//  CountryTableCell.swift
//  CurrencyConvertor
//
//  Created by Kaodim MacMini on 21/01/2021.
//

import UIKit

class CountryViewViewModel {
    var currency: Currency

    var id: String {
        currency.id
    }

    var name: String {
        currency.name
    }

    var fullName: String {
        currency.fullName
    }

    init(_ currency: Currency) {
        self.currency = currency
    }
}

class CountryTableCell: UITableViewCell {

    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var countryName: UILabel!
    @IBOutlet weak var countryFlag: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        countryFlag.layer.cornerRadius = 10
    }

    func configureUI(_ currency: CountryViewViewModel) {
        self.currencyLabel.text = currency.name
        self.countryName.text = currency.fullName
        self.countryFlag.image = UIImage(named: currency.name.lowercased())
    }
}
