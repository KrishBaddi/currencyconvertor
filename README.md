<h2> Currency convertor App using MVVM and Combine</h2>
<h6>  This app is a currency convertor app that uses Exchange Rate API to fetch rates </h6>
</br>

</br>

<h2>Software required and dependency</h2>

<ul>
  <li>Xcode 12.3 </li>
  <li>iOS Deployment target 14 </li>
</ul>

<h2>Please use Exchange Rate API to fetch rates </h2>
<ul>
 <li>https://www.exchangerate-api.com/</li>
</ul>

<h2>What we will developed.</h2>
<ul>
 <li>Fetch latest exchange rate and save to local database</li>
 <li>Currency conversion</li>
 <li>Select country sectioned by continents </li>
<li> Refresh button incase of network failure </li>
</ul>


<h2>Repository Branches</h2>
<ul>
 <li>Master</li>
</ul>

<h2>Future Work </h2>
<ul>
 <li> Swapping currency </li>
 <li> Add unit testing </li>
</ul>




